use crate::*;

// TODO include prefix in RevocationPayload and use below
// use crate::runtime::runtime_types::pallet_identity::types::RevocationPayload;
type EncodedRevocationPayload = Vec<u8>;

pub fn print_revoc_sig(data: &Data) {
	let (_, signature) = generate_revoc_doc(data);
	println!("revocation payload signature");
	println!("0x{}", hex::encode(signature));
}

pub fn generate_revoc_doc(data: &Data) -> (EncodedRevocationPayload, sr25519::Signature) {
	let payload = (b"revo", data.genesis_hash, data.idty_index()).encode();
	let KeyPair::Sr25519(keypair) = data.keypair() else {
		panic!("Cesium keys not implemented there")
	};
	let signature = keypair.sign(&payload);

	(payload, signature)
}
