// commands/publish.rs
// This module handles the 'publish' command of the CLI.

use crate::GcliError;
use anyhow::anyhow;
use inquire::Confirm;
use std::process::Command;

/// Executes the 'publish' operation.
pub async fn handle_command() -> Result<(), GcliError> {
	// Step 1: Get actual version of gcli
	const VERSION: &str = env!("CARGO_PKG_VERSION");

	// Fetch the latest tags from the remote repository
	Command::new("git")
		.args(["fetch", "--tags"])
		.status()
		.map_err(|e| anyhow!(e))?;

	// Step 2: Check if the git tag already exists
	let tag_check_output = Command::new("git").args(["tag", "-l", VERSION]).output()?;

	if !tag_check_output.stdout.is_empty() {
		return Err(GcliError::Logic(format!("Tag {VERSION} already exists")));
	}

	// Display a confirmation prompt with the version number.
	match Confirm::new(&format!(
		"Are you sure you want to publish version {VERSION} ?"
	))
	.with_default(false)
	.prompt()
	{
		Ok(true) => {
			// User confirmed, proceed publishing
			// Step 3: Create and push the git tag

			Command::new("git")
				.args(["tag", "-a", VERSION, "-m", &format!("Release v{VERSION}")])
				.status()
				.map_err(|e| anyhow!(e))?;

			Command::new("git")
				.args(["push", "origin", &format!("refs/tags/{VERSION}")])
				.status()
				.map_err(|e| anyhow!(e))?;
			println!("Publication of version {VERSION} completed successfully.");
			Ok(())
		}
		Ok(false) => {
			// User did not confirm, cancel the operation
			println!("Publication cancelled.");
			Ok(())
		}
		Err(_) => {
			// There was an error with the prompt, return an error
			Err(GcliError::Input(
				"Failed to display confirmation prompt".to_string(),
			))
		}
	}
}
